<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


// On récupère l'action passée dans l'URL

// Appel de la méthode statique $action de ControleurUtilisateur
if (isset($_GET['action'])) {
    $listeActions = get_class_methods("App\\Covoiturage\\Controleur\\ControleurUtilisateur");
    $action = $_GET['action'];
    if (in_array($action, $listeActions)) {
        ControleurUtilisateur::$action();
    } else {
        ControleurUtilisateur::afficherErreur("Action inexistante");
    }

} else {
    ControleurUtilisateur::afficherListe();
}
?>
