<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(string $chemin = "", array $parametres = []): void
    {
        $utilisateurs = AbstractDataObject::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        if (!$chemin) {
            self::afficherVue("vueGenerale.php", [$utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        } else {
            self::afficherVue("vueGenerale.php", [$utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/$chemin", $parametres]);
        }
    }

    public static function afficherDetail(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']); //appel au modèle pour gérer la BD
        if ($utilisateur) {
            self::afficherVue("vueGenerale.php", [$utilisateur, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            self::afficherVue("vueGenerale.php", ["titre" => "Problème avec l'utilisateur", "cheminCorpsVue" => "utilisateur/ereur.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Se créer un compte utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = new Utilisateur($_GET["login"], $_GET["nom"], $_GET["prenom"]);
        AbstractRepository::ajouter($utilisateur);

        self::afficherListe("utilisateurCree.php");
    }

    public static function supprimer(): void
    {
        $utilisateurLogin = $_GET["login"];
        AbstractRepository::supprimerParLogin($utilisateurLogin);

        self::afficherListe("utilisateurSupprime.php", [$utilisateurLogin]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }


    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }



}

?>

