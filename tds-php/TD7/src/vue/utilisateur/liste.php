
<?php
/** @var ModeleUtilisateur[] $parametres */

$utilisateurs = $parametres[0];
foreach ($utilisateurs as $utilisateur) {
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo "<p> Utilisateur de login <a href='?action=afficherDetail&login={$loginURL}'>" . htmlspecialchars($utilisateur->getLogin()) . "</a>. <a href='?action=supprimer&login={$loginURL}'>Supprimer l'utilisateur</a></p>";
}
?>
<p><a href="?action=afficherFormulaireCreation">Créer un utilisateur</a></p>
