<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

class UtilisateurRepository
{
    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager) {
            return $this->trajetsCommePassager;
        }
        $this->trajetsCommePassager = self::recupererTrajetsCommePassager();
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau["login"],
            $utilisateurFormatTableau["nom"],
            $utilisateurFormatTableau["prenom"]
        );
    }

    public static function recupererUtilisateurs(): array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM utilisateur";

        $pdoStatement = $pdo->query($sql);

        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login): ?Utilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau) {
            return UtilisateurRepository::construireDepuisTableauSQL($utilisateurFormatTableau);
        } else {
            return null;
        }
    }

    public static function ajouter(Utilisateur $utilisateur): bool
    {
        try {
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";

            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "login" => $utilisateur->getLogin(),
                "nom" => $utilisateur->getNom(),
                "prenom" => $utilisateur->getPrenom()
            );

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return false;
    }

    public static function supprimerParLogin($login): bool
    {
        try {
            $sql = "DELETE FROM utilisateur WHERE login=:login";

            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "login" => $login
            );

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * @return Trajet[]
     */
    public function recupererTrajetsCommePassager(): array
    {

        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM trajet t INNER JOIN passager p ON p.trajetId = t.id WHERE passagerLogin = :login";

        $pdoStatement = $pdo->prepare($sql);

        $values = array(
            "login" => $this->login
        );

        $pdoStatement->execute($values);
        $trajets = [];

        foreach ($pdoStatement as $trajet) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);

        }
        return $trajets;
    }
}