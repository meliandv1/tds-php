<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Utilisateur;
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(bool $apresInscription = False): void
    {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        if (!$apresInscription) {
            self::afficherVue("vueGenerale.php", [$utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        } else {
            self::afficherVue("vueGenerale.php", [$utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        }
    }

    public static function afficherDetail(): void
    {
        $utilisateur = Utilisateur::recupererUtilisateurParLogin($_GET['login']); //appel au modèle pour gérer la BD
        if ($utilisateur) {
            self::afficherVue("vueGenerale.php", [$utilisateur, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            self::afficherVue("vueGenerale.php", ["titre" => "Problème avec l'utilisateur", "cheminCorpsVue" => "utilisateur/ereur.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Se créer un compte utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = new Utilisateur($_GET["login"], $_GET["nom"], $_GET["prenom"]);
        $utilisateur->ajouter();

        self::afficherListe(True);
    }


    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }


}

?>

