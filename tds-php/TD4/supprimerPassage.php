<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div>
    <form method="get" action="supprimerPassage.php">
        <fieldset>
            <legend>Désinscrire l'utilisateur du trajet :</legend>
            <p>
                <label for="login_id">Login</label> :
                <input type="text" placeholder="Ex : leblancj" name="login" id="login_id" required/>
                <label for="login_id">trajet_id</label> :
                <input type="text" placeholder="Ex : 9" name="id" id="trajet_id" required/>
            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

function desinscrire(string $login, string $id) : void {
    echo $login . $id;
    echo Trajet::recupererTrajetParId($id)->supprimerPassager($login);
}

if (isset($_GET['login'], $_GET['id'])) {
    $u = desinscrire($_GET['login'], $_GET['id']);
    echo $u;
}
?>
</body>
</html>