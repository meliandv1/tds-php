<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>  </title>
</head>

<body>
<p>Le nouveau trajet :
<?php

use Modele\ModeleUtilisateur;

require_once "Trajet.php";
require_once "Utilisateur.php";
$trajet = new Trajet(null,$_POST['depart'],$_POST['arrivee'],new DateTime($_POST['date']),$_POST['prix'],ModeleUtilisateur::recupererUtilisateurParLogin($_POST['conducteurLogin']),isset($_POST['nonFumeur']));
$trajet->ajouter();

echo $trajet;
?>
</p>
</body>
</html>