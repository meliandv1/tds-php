<?php

require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue("utilisateur/liste.php", [$utilisateurs]);
    }

    public static function afficherDetail() : void {
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']); //appel au modèle pour gérer la BD
        if($utilisateur) {
            self::afficherVue("utilisateur/detail.php", [$utilisateur]);
        } else {
            self::afficherVue("utilisateur/erreur.php");
        }
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue("utilisateur/formulaireCreation.php");
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = new ModeleUtilisateur($_GET["login"],$_GET["nom"],$_GET["prenom"]);
        $utilisateur->ajouter();

        self::afficherListe();
    }



    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }


}
?>

