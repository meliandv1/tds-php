<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Detail de l'utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $parametres */

use Modele\ModeleUtilisateur;

$utilisateur = $parametres[0];

echo '<p> Utilisateur de login ' . $utilisateur->getLogin() . ' de prénom ' . $utilisateur->getPrenom() . ' et de nom ' . $utilisateur->getNom() . '.</p>';
?>
</body>
</html>