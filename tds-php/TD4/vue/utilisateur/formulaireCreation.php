<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>

<body>
<form method="get" action="?action=creerDepuisFormulaire">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="login_id">Login</label> :
            <input type="text" placeholder="leblancj" name="login" id="login_id" required/>
            <br><br>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="Leblanc" name="nom" id="nom_id" required/>
            <br><br>
            <label for="prenom_id">Prénom</label> :
            <input type="text" placeholder="Juste" name="prenom" id="prenom_id" required/>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>


        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <!--
            4) Le clic sur le bouton va charger l'url avec les informations dans l'url
               grace à la requête HTTP de type GET suivante :
               GET /~Vincent/creerUtilisateur.php?login=leblancj&nom=Leblanc&prenom=Juste HTTP/1.1
               Cette partie de l'url s'appelle le query string
            -->
        </p>
    </fieldset>
</form>
</body>
</html>
