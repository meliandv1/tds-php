<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $parametres */

use Modele\ModeleUtilisateur;

$utilisateurs = $parametres[0];
foreach ($utilisateurs as $utilisateur)
    echo "<p> Utilisateur de login <a href='?action=afficherDetail&login={$utilisateur->getLogin()}'>" . $utilisateur->getLogin() . '</a>.</p>';
?>
<p><a href="?action=afficherFormulaireCreation">Créer un utilisateur</a></p>
</body>
</html>