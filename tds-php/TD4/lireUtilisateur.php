<?php

use Modele\ModeleUtilisateur;

require_once "Utilisateur.php";

$utilisateurs = ModeleUtilisateur::recupererUtilisateurs();


foreach ($utilisateurs as $utilisateur) {
    //echo $utilisateur->__toString(); pareil que :
    echo $utilisateur . " a les trajets : ";
    foreach ($utilisateur->getTrajetsCommePassager() as $trajet) {
        echo $trajet . "<br>";
    }
}

//$utilisateur = Utilisateur::recupererParClePrimaire("test");
//var_dump($utilisateur->recupererTrajetsCommePassager());
?>