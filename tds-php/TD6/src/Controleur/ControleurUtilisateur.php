<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(string $chemin = "", array $parametres = []): void
    {
        $utilisateurs = (new UtilisateurRepository)->recuperer(); //appel au modèle pour gérer la BD
        if (!$chemin) {
            self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        } else {
            self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => $parametres["titre"], "cheminCorpsVue" => "utilisateur/$chemin", "sousParametres" => $parametres]);
        }
    }

    public static function afficherDetail(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']); //appel au modèle pour gérer la BD
        if ($utilisateur) {
            self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            self::afficherVue("vueGenerale.php", ["titre" => "Problème avec l'utilisateur", "cheminCorpsVue" => "utilisateur/ereur.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Se créer un compte utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($utilisateur);

        self::afficherListe("utilisateurCree.php", ["titre" => "Utilisateur crée !"]);
    }

    public static function supprimer(): void
    {
        $utilisateurLogin = $_GET["login"];
        (new UtilisateurRepository())->supprimer($utilisateurLogin);

        self::afficherListe("utilisateurSupprime.php", ["utilisateur" => $utilisateurLogin, "titre" => "Utilisateur supprimé !"]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateurLogin = $_GET["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($utilisateurLogin);
        self::afficherVue("vueGenerale.php", ["titre" => "Se créer un compte utilisateur", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
    }

    public static function mettreAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET["login"]);
        $ancienLogin = $utilisateur->getLogin();
        $utilisateur->setLogin($_GET["login"]);
        $utilisateur->setNom($_GET["nom"]);
        $utilisateur->setPrenom($_GET["prenom"]);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        self::afficherListe("utilisateurMisAJour.php", ["login" => $utilisateur->getLogin(), "titre" => "Utilisateur mis à jour"]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }


    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"]);
        return $utilisateur;
    }

    public static function deposerCookie() {
        Cookie::enregistrer("TestCookie", 12345, time() + 3600);
    }
    public static function lireCookie() {
        echo Cookie::lire("TestCookie");
    }
}

?>

