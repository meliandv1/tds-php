<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;
class ControleurTrajet
{
    public static function afficherListe(string $chemin = "", array $parametres = []): void
    {
        $trajets = (new TrajetRepository)->recuperer(); //appel au modèle pour gérer la BD
        if (!$chemin) {
            self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);
        } else {
            self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => $parametres["titre"], "cheminCorpsVue" => "trajet/$chemin", "sousParametres" => $parametres]);
        }
    }

    public static function afficherDetail(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['trajetid']); //appel au modèle pour gérer la BD
        if ($trajet) {
            self::afficherVue("vueGenerale.php", ["trajet" => $trajet, "titre" => "Detail des trajets", "cheminCorpsVue" => "trajet/detail.php"]);
        } else {
            self::afficherVue("vueGenerale.php", ["titre" => "Problème avec le trajet", "cheminCorpsVue" => "trajet/ereur.php"]);
        }
    }
    public static function supprimer(): void
    {
        $trajetId = $_GET["trajetid"];
        (new TrajetRepository)->supprimer($trajetId);

        self::afficherListe("trajetSupprime.php", ["trajetId" => $trajetId, "titre" => "Trajet supprimé !"]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Créer un trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::constuireDepuisFormulaire($_GET);
        (new TrajetRepository)->ajouter($trajet);

        self::afficherListe("trajetCree.php", ["titre" => "Trajet crée !"]);
    }
    public static function afficherFormulaireMiseAJour(): void
    {
        $trajetId = $_GET["trajetid"];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($trajetId);
        self::afficherVue("vueGenerale.php", ["titre" => "Créer un trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
    }

    public static function mettreAJour(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET["trajetid"]);
        $trajet->setDepart($_GET["depart"]);
        $trajet->setArrivee($_GET["arrivee"]);
        $trajet->setDate(new DateTime($_GET["date"]));
        $trajet->setPrix($_GET["prix"]);
        $trajet->setConducteur((new UtilisateurRepository)->recupererParClePrimaire($_GET["conducteur"]));
        $trajet->setNonFumeur(isset($_GET["prix"]));
        (new TrajetRepository())->mettreAJour($trajet);
        self::afficherListe("trajetMisAJour.php", ["id" => $trajet->getId(), "titre" => "Trajet mis à jour"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    /**
     * @return Trajet
     * @throws \Exception
     */
    private static function constuireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $trajet = new Trajet($tableauDonneesFormulaire["id"] ?? null, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], new DateTime($tableauDonneesFormulaire['date']), $tableauDonneesFormulaire['prix'], (new UtilisateurRepository)->recupererParClePrimaire($tableauDonneesFormulaire['conducteur']), isset($tableauDonneesFormulaire['nonfumeur']));
        return $trajet;
    }
}