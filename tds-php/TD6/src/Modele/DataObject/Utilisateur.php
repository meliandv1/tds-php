<?php

namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class Utilisateur extends AbstractDataObject
{


    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
        array  $trajetsCommePassager = null
    )
    {
        $this->login = substr($login, 0, 64);;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString(): string {
        return "<ul><li>$this->login</li><li>$this->prenom</li><li>$this->nom</li></ul>";
    }*/

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return substr($this->login, 0, 64);
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }


}

?>