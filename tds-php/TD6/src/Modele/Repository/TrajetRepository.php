<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;


class TrajetRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ?
        );
        $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
        return $trajet;
    }


    private function recupererPassagers($trajet) : array {
        $sql = "SELECT passagerLogin FROM passager p INNER JOIN utilisateur u ON u.login = p.passagerLogin WHERE p.trajetId = :id";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "id" => $trajet->getId()
        );

        $pdoStatement->execute($values);
        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = (new UtilisateurRepository())->recupererParClePrimaire($utilisateurFormatTableau["passagerLogin"]);
        }

        return $utilisateurs;
    }


    protected function getNomTable(): string
    {
        return "trajet";
    }
    protected function getNomClePrimaire(): string
    {
        return "id";
    }
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }
    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format('Y-m-d'),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur() ? "1" : "0"
        );
    }

}