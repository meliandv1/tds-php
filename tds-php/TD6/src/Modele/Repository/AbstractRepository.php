<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;
use PDOException;

abstract class AbstractRepository
{


    public function mettreAJour(AbstractDataObject $objet): void
    {
        $tokens = $this->formatTableauSQL($objet);

        $keys = array_keys($tokens);
        $token = 0;
        $requete = "";
        foreach ($this->getNomsColonnes() as $colonne) {
            $requete .= $colonne . " = :" . $keys[$token];
            $token += 1;
            if ($token < count($tokens)) {
                $requete .= ", ";
            }
        }
        $sql = "UPDATE " . $this->getNomTable() . " SET " . $requete . " WHERE " . $this->getNomClePrimaire() . " = :" . $keys[0];

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = [];
        foreach ($keys as $key) {
            $values[":" . $key] = $tokens[$key];
        }
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        try {
            $sql = "INSERT INTO " . $this->getNomTable() . " (" . join(", ", $this->getNomsColonnes()) . ") VALUES (:" . join(", :", $this->getNomsColonnes()) . ")";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $tokens = $this->formatTableauSQL($objet);

            $keys = array_keys($tokens);
            $token = 0;

            $values = [];
            foreach ($this->getNomsColonnes() as $colonne) {
                $values[":" . $colonne] = $tokens[$keys[$token]];
                $token += 1;
            }

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return false;
    }

    public function supprimer($valeurClePrimaire): bool
    {
        try {
            $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() .  " = :valeurClePrimaire";

            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "valeurClePrimaire" => $valeurClePrimaire,
            );

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function recupererParClePrimaire(string $valeurClePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :valeurClePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire,
        );
        $pdoStatement->execute($values);

        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau) {
            return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
        } else {
            return null;
        }
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM " . $this->getNomTable());

        $utilisateurs = [];
        $nomInstance = "App\\Covoiturage\\Modele\\Repository\\" . ucfirst($this->getNomTable()) . "Repository";
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = (new $nomInstance)->construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire(): string;
    protected abstract function getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;


}
?>