<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;


class UtilisateurRepository extends AbstractRepository
{
    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager) {
            return $this->trajetsCommePassager;
        }
        $this->trajetsCommePassager = self::recupererTrajetsCommePassager();
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau["login"],
            $utilisateurFormatTableau["nom"],
            $utilisateurFormatTableau["prenom"]
        );
    }

    /**
     * @return Trajet[]
     */
    public function recupererTrajetsCommePassager(): array
    {

        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM trajet t INNER JOIN passager p ON p.trajetId = t.id WHERE passagerLogin = :login";

        $pdoStatement = $pdo->prepare($sql);

        $values = array(
            "login" => $this->login
        );

        $pdoStatement->execute($values);
        $trajets = [];

        foreach ($pdoStatement as $trajet) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);

        }
        return $trajets;
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }
    protected function getNomClePrimaire(): string
    {
        return "login";
    }
    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }
    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }




}