<?php

/**
 * @var array[] $parametres
 */
$trajets = $parametres["trajets"];
foreach ($trajets as $trajet) {
    $idURL = rawurlencode($trajet->getId());

    echo "<p> Trajet allant de " . htmlspecialchars($trajet->getDepart()) ." à " . htmlspecialchars($trajet->getArrivee()) . ". <a href='?controleur=Trajet&action=afficherDetail&trajetid={$idURL}'>" . "Détail sur le trajet" . "</a>. <a href='?controleur=Trajet&action=supprimer&trajetid={$idURL}'>Supprimer le trajet</a> <a href='?controleur=Trajet&action=afficherFormulaireMiseAJour&trajetid={$idURL}'>Mettre à jour le trajet</a></p>";
}
?>
<p><a href="?controleur=Trajet&action=afficherFormulaireCreation">Créer un trajet</a></p>
