<?php

/** @var array[] $parametres */
$trajet = $parametres["trajet"];

?>

<form method="get" action="?controleur=Utilisateur&action=creerDepuisFormulaire">
    <fieldset class="Input">

        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="depart_id">Depart</label> :
            <input value="<?= htmlspecialchars($trajet->getDepart()); ?>" class="InputAddOn-field" type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="arrivee_id">Nom</label> :
            <input value="<?= htmlspecialchars($trajet->getArrivee()); ?>" class="InputAddOn-field" type="text" placeholder="Marseille" name="arrivee" id="arrivee_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="date_id">Date</label> :
            <input value="<?= htmlspecialchars($trajet->getDate()->format('Y-m-d')); ?>" class="InputAddOn-field" type="date" name="date" id="date_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="prix_id">Prix</label> :
            <input value="<?= htmlspecialchars($trajet->getPrix()); ?>" class="InputAddOn-field" type="number" placeholder="20" name="prix" id="prix_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="conducteur_id">Conducteur</label> :
            <input value="<?= htmlspecialchars($trajet->getConducteur()->getLogin()); ?>" class="InputAddOn-field" type="text" placeholder="leblancj" name="conducteur" id="conducteur_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="nonfumeur_id">Non fumeur</label> :
            <input value="<?= htmlspecialchars($trajet->isNonFumeur()); ?>" class="InputAddOn-field" type="checkbox" name="nonfumeur" id="nonfumeur_id" checked/>
        </div>
        <input class="InputAddOn-button" type='hidden' name='trajetid' value='<?= htmlspecialchars($trajet->getId()); ?>'>

        <input class="InputAddOn-button" type='hidden' name='action' value='mettreAJour'>

        <input class="InputAddOn-button" type='hidden' name='controleur' value='Trajet'>


        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <!--
4) Le clic sur le bouton va charger l'url avec les informations dans l'url
               grace à la requête HTTP de type GET suivante :
               GET /~Vincent/creerUtilisateur.php?login=leblancj&nom=Leblanc&prenom=Juste HTTP/1.1
               Cette partie de l'url s'appelle le query string
-->
        </p>
    </fieldset>
</form>
