<form method="get" action="?controleur=Trajet&action=creerDepuisFormulaire">
    <fieldset class="Input">

        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="depart_id">Depart</label> :
            <input class="InputAddOn-field" type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee</label> :
            <input class="InputAddOn-field" type="text" placeholder="Marseille" name="arrivee" id="arrivee_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="date_id">Date</label> :
            <input class="InputAddOn-field" type="date" name="date" id="date_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="prix_id">Prix</label> :
            <input class="InputAddOn-field" type="number" placeholder="20" name="prix" id="prix_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="conducteur_id">Conducteur</label> :
            <input class="InputAddOn-field" type="text" placeholder="leblancj" name="conducteur" id="conducteur_id" required/>
        </div>

        <div class="InputAddOn-element">
            <label class="InputAddOn-item" for="nonfumeur_id">Non fumeur</label> :
            <input class="InputAddOn-field" type="checkbox" name="nonfumeur" id="nonfumeur_id" checked/>
        </div>

        <input class="InputAddOn-button" type='hidden' name='controleur' value='Trajet'>

        <input class="InputAddOn-button" type='hidden' name='action' value='creerDepuisFormulaire'>


        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <!--
            4) Le clic sur le bouton va charger l'url avec les informations dans l'url
               grace à la requête HTTP de type GET suivante :
               GET /~Vincent/creerUtilisateur.php?login=leblancj&nom=Leblanc&prenom=Juste HTTP/1.1
               Cette partie de l'url s'appelle le query string
            -->
        </p>
    </fieldset>
</form>