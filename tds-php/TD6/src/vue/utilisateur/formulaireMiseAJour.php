<?php

/** @var array[] $parametres */
$utilisateur = $parametres["utilisateur"];

?>

<form method="get" action="?controleur=Utilisateur&action=creerDepuisFormulaire">
    <fieldset class="Input">

        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <div class="InputAddOn-element">
                <label class="InputAddOn-item" for="login_id">Login</label> :
                <input value="<?= htmlspecialchars($utilisateur->getLogin()); ?>" class="InputAddOn-field" type="text" placeholder="leblancj" name="login" id="login_id" required/>
            </div>

            <div class="InputAddOn-element">
                <label class="InputAddOn-item" for="nom_id">Nom</label> :
                <input value="<?= htmlspecialchars($utilisateur->getNom()); ?>" class="InputAddOn-field" type="text" placeholder="Leblanc" name="nom" id="nom_id" required/>
            </div>

            <div class="InputAddOn-element">
                <label class="InputAddOn-item" for="prenom_id">Prénom</label> :
                <input value="<?= htmlspecialchars($utilisateur->getPrenom()); ?>" class="InputAddOn-field" type="text" placeholder="Juste" name="prenom" id="prenom_id" required/>
            </div>
            <input class="InputAddOn-button" type='hidden' name='action' value='mettreAJour'>

        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <!--
4) Le clic sur le bouton va charger l'url avec les informations dans l'url
               grace à la requête HTTP de type GET suivante :
               GET /~Vincent/creerUtilisateur.php?login=leblancj&nom=Leblanc&prenom=Juste HTTP/1.1
               Cette partie de l'url s'appelle le query string
-->
        </p>
    </fieldset>
</form>
