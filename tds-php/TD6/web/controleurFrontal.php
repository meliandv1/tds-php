<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$controleur = isset($_GET['controleur']) ? $_GET['controleur'] : "Utilisateur";
$nomDeClasseControleur = "App\\Covoiturage\\Controleur\\Controleur".ucfirst($controleur);

if (class_exists($nomDeClasseControleur)) {
    // Appel de la méthode statique $action de ControleurUtilisateur
    if (isset($_GET['action'])) {

        $listeActions = get_class_methods($nomDeClasseControleur);
        $action = $_GET['action'];

        if (in_array($action, $listeActions)) {
            $nomDeClasseControleur::$action();
        } else {
            ControleurUtilisateur::afficherErreur("Action inexistante");
        }
    } else {
        ControleurUtilisateur::afficherErreur("Erreur interne : la classe controleur est sensé être utilisateur par défaut");
    }
} else {
    ControleurUtilisateur::afficherErreur("Controleur inexistant");
}


?>
