<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
            $texte = "hello world !<br>";

          // On écrit le contenu de la variable 'texte' dans la page Web

            echo "$texte \n";
            /*
            $user = [
                'prenom' => 'Juste',
                'nom' => 'Leblanc',
            ];
            $user["passion"] = "maquettes";
            $user[] = "Nouvelle valeur";
            foreach ($user as $ref => $valeur){
                echo "la ref $ref à pour valeur : $valeur \n";
            };
            $id = [];

            $id[]= "test1";
            $id[]= "test2";

            for ($i = 0; $i < count(array_keys($user)); $i++) {
                $cle = array_keys($user)[$i];
                $valeur = $user[$cle];
                echo "$valeur \n";
            }
            var_dump($user);
            var_dump(array_keys($user));
        */
        //ex 8
        /*
            $prenom = "Juste";
            $nom = "Leblanc";
            $login = "leblancj";

            echo "<p> Utilisateur {$prenom} {$nom} de login {$login} </p>"

            $utilisateur = [
                "prenom" => "Juste",
                "nom" => "Leblanc",
                "login" => "leblancj"
            ];
            //var_dump($utilisateur);
            echo "<p> Utilisateur {$utilisateur["prenom"]} {$utilisateur["nom"]} de login {$utilisateur["login"]} </p>";
        */
        $utilisateurs = [
            [
                "prenom" => "Juste",
                "nom" => "Leblanc",
                "login" => "leblancj",
            ],
            [
                "prenom" => "Celestin",
                "nom" => "Duchancle",
                "login" => "duchanclec",
            ],
            [
                "prenom" => "Alex",
                "nom" => "Alexander",
                "login" => "alexandera",
            ]
        ];
        //var_dump($utilisateurs);
        if (count(array_keys($utilisateurs)) == 0) {
            echo "Il n'y a aucun utilisateur.";
        } else {
            foreach ($utilisateurs as $i => $utilisateur) {
                echo "<ul>";
                foreach ($utilisateur as $e => $valeur) {

                    echo "<li>$valeur</li>";
                };
                echo "</ul>";
            };
        }
        ?>
    </body>
</html>