<!DOCTYPE html>
<html>
<head>
    <title> Insérer le titrer ici </title>
    <meta charset="utf-8" />
</head>

<body>
    <?php

    use Modele\ModeleUtilisateur;

    require_once "Utilisateur.php";
    $utilisateur1 = new ModeleUtilisateur("leblancj","Leblanc","Juste");
    echo $utilisateur1;
    $utilisateur2 = new ModeleUtilisateur("Seulement 64 caractères affichés 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20","Leblanc","Juste");
    echo $utilisateur2;
    $utilisateur2 = new ModeleUtilisateur("leblancj",["le","blanc"],"Juste");
    echo $utilisateur2;
    ?>
</body>
</html>


