<?php

use Modele\ConnexionBaseDeDonnees;

require_once "ConnexionBaseDeDonnees.php";

require_once "Trajet.php";
class ModeleUtilisateur {


    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom(): string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
        array $trajetsCommePassager = null
   ) {
        $this->login = substr($login, 0, 64);;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString(): string {
        return "<ul><li>$this->login</li><li>$this->prenom</li><li>$this->nom</li></ul>";
    }*/
    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager) {
            return $this->trajetsCommePassager;
        }
        $this->trajetsCommePassager = self::recupererTrajetsCommePassager();
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }
    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return substr($this->login, 0, 64);
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Modele\ModeleUtilisateur {
        return new Modele\ModeleUtilisateur(
            $utilisateurFormatTableau["login"],
            $utilisateurFormatTableau["nom"],
            $utilisateurFormatTableau["prenom"]
        );
    }
    
    public static function recupererUtilisateurs() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM utilisateur";

        $pdoStatement = $pdo->query($sql);

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Modele\ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
    
        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    
        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        
        if ($utilisateurFormatTableau) {
            return Modele\ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        } else {
            return null;
        }
    }

    public function ajouter() : bool {
        try {
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";

            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "login" => $this->login,
                "nom" => $this->nom,
                "prenom" => $this->prenom
            );

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return false;

    }

    /**
     * @return Trajet[]
     */
    public function recupererTrajetsCommePassager() : array
    {

        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM trajet t INNER JOIN passager p ON p.trajetId = t.id WHERE passagerLogin = :login";

        $pdoStatement = $pdo->prepare($sql);

        $values = array(
            "login" => $this->login
        );

        $pdoStatement->execute($values);
        $trajets = [];

        foreach ($pdoStatement as $trajet) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);

        }
        return $trajets;
    }
}
?>